# Charles URL Shortener Frontend

## 範例

[短網址 Dashboard](https://charles-url-shortener.netlify.app)

## Introduction

短網址是以行銷角度發想，行銷角度常會有需要推廣一頁式網站。因貼在社群網站上很有機會被舉報或封鎖。
短網址出現就可以解決這項問題，正式的 Domain 可以被隱藏起來。

## Screenshot

![alt cover](/public/screenshot.jpg)

## 開發環境

- Node Version - v20.9.0
- NVM Version - v0.35.2
- NPM Version - v10.1.0
- YARN Version - v1.22.21

### 環境安裝

[安裝 Node 教學](https://www.jb51.net/article/283896.htm)
[安裝 nvm 教學](https://www.ruyut.com/2023/05/linux-nvm.html)

### 執行專案

```
# 安裝所需要的套件
yarn

# 在 Local 端執行
yarn dev
```

## Use Technology & Library

- Vite + React
- Axios

## Features

- [x] 新增短網址
- [x] 刪除短網址
- [x] 短網址點擊率
