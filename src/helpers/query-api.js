import { request } from "./httpRequest";

export const addShortenerURL = (url) => {
  return request("POST", "/shortUrls/create", { fullUrl: url });
};

export const removeShortenerURL = (id) => {
  return request("POST", "/shortUrls/delete", { shortUrlId: id });
};

export const fetchShortenerURL = (id) => {
  return request("GET", "/shortUrls/query", { shortUrlId: id });
};
