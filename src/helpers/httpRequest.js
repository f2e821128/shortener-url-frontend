import axios from "axios";
import parseError from "./parseError";

const DOMAIN = `${import.meta.env.VITE_BACKEND_URL}`;

const instance = axios.create({
  baseURL: DOMAIN,
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  timeout: 1000,
});

instance.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    // Do something with response data
    return { status: response.status, data: response.data };
  },
  function (error) {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          // Unauthorized User
          return "Error 401 Unauthorized";
        case 404:
          // go to 404 page
          return "Error 404 Not Found";
        case 500:
          // go to 500 page
          return "Error 404 Internal Server Error";
        default:
          return parseError(error.message);
      }
    }
    if (!window.navigator.onLine) {
      return "ERR_INTERNET_DISCONNECTED";
    }
    return Promise.reject(error);
  }
);

function request(method, url, data = null, config) {
  method = method.toLowerCase();
  switch (method) {
    case "get":
      return instance.get(url, { params: data });
    case "post":
      return instance.post(url, data, config);
    case "delete":
      return instance.delete(url, { params: data });
    case "put":
      return instance.put(url, data);
    case "patch":
      return instance.patch(url, data);
    default:
      return false;
  }
}

export { request };
