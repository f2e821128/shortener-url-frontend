import { useState } from "react";
import { addShortenerURL } from "../helpers/query-api.js";

function ShortenerURLForm({ query }) {
  const [url, setURL] = useState("");
  const [invaild, setInvaild] = useState(false);

  const createShortenerURL = () => {
    const result = isValidHttpUrl(url);
    if (result) {
      setInvaild(false);
      addShortenerURL(url)
        .then(() => {
          query();
          setURL("");
        })
        .catch((err) => console.error({ err }));
    } else {
      setInvaild(true);
    }
  };

  const clearShortenerURL = () => {
    setURL("");
    setInvaild(false);
  };

  const change = (e) => {
    setURL(e.target.value);
  };

  const isValidHttpUrl = (string) => {
    try {
      new URL(string);
      return true;
    } catch (err) {
      return false;
    }
  };

  return (
    <section className="border-b border-gray-900/10" action="#">
      <div className="space-y-12">
        <div className=" pb-12 flex flex-col items-center justify-center space-y-8 mt-4">
          <h1 className="text-4xl font-semibold leading-7 text-gray-900"> Charles URL Shortener </h1>
          <p className="mt-1 text-sm leading-6 text-gray-600">Here can help you generate shortener url.</p>

          <div className="flex flex-col items-center justify-center mt-10 gap-x-6 gap-y-8 sm:grid-cols-6 w-full ">
            <div className="sm:col-span-4 w-96">
              <label htmlFor="username" className="block text-sm font-medium leading-6 text-gray-900">
                Original URL
              </label>
              <div className="mt-2">
                <div className="flex rounded-md p-1 shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                  <input
                    type="url"
                    name="shortener-url"
                    id="shortener-url"
                    autoComplete="off"
                    className="block flex-1 border-0 bg-transparent py-2 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 focus:outline-none"
                    placeholder="https://"
                    value={url}
                    onChange={change}
                  />
                </div>
                <small className={"text-red-400 text-xs " + (invaild ? "show" : "hidden")}> Invaild URL </small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="my-6 flex items-center justify-end gap-x-6 w-96 mx-auto">
        <button type="button" className="text-sm font-semibold leading-6 text-gray-900" onClick={clearShortenerURL}>
          Clear
        </button>
        <button
          type="submit"
          className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          onClick={createShortenerURL}>
          Create
        </button>
      </div>
    </section>
  );
}

export default ShortenerURLForm;
