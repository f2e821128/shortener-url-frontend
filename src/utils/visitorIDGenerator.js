import * as FingerprintJS from "./fp.esm.js";

async function getVisitorData() {
  const fp = await FingerprintJS.load();
  return await fp.get();
}

async function visitorIDGenerator() {
  try {
    const isGoogleBot = navigator.userAgent.includes("compatible") || navigator.userAgent.includes("Googlebot");
    let vistiorID = "";
    if (isGoogleBot) {
      vistiorID = "GoogleBot";
    }
    if (!isGoogleBot) {
      const key1 = await getVisitorData();
      const key2 = await getVisitorData();
      const key3 = await getVisitorData();
      const key4 = await getVisitorData();
      const key5 = await getVisitorData();

      const KEYARRAY = [`${key1.visitorId}`, `${key2.visitorId}`, `${key3.visitorId}`, `${key4.visitorId}`, `${key5.visitorId}`];
      const FILTEREDKEY = KEYARRAY.filter((v, i) => KEYARRAY.indexOf(v) !== i);
      const UNIQUEKEY = new Set(FILTEREDKEY);
      vistiorID = `${Array.from(UNIQUEKEY)[0]}`;
    }
    return vistiorID;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

export default visitorIDGenerator;
