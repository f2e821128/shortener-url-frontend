import ReactDOM from "react-dom/client";
import ClientApplication from "./ClientApplication.jsx";
import App from "./App.jsx";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(
  <ClientApplication>
    <App />
  </ClientApplication>
);
