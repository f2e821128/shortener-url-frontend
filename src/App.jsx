import { useEffect, useState } from "react";
import { fetchShortenerURL } from "./helpers/query-api.js";
import ShortenerURLForm from "./components/ShortenerURLForm";
import DataTable from "./components/DataTable";

function App() {
  const [shortURLInfo, setShortURLInfo] = useState([]);

  const queryShortenerURL = () => {
    fetchShortenerURL()
      .then((response) => {
        const { shortUrls } = response.data;
        setShortURLInfo(shortUrls);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    queryShortenerURL();
  }, []);

  return (
    <main>
      <ShortenerURLForm query={queryShortenerURL} />
      <DataTable shortURL={shortURLInfo} query={queryShortenerURL} />
    </main>
  );
}

export default App;
